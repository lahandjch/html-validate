module.exports = {
	rules: {
		"input-missing-label": "error",
		"heading-level": "error",
		"missing-doctype": "error",
		"no-missing-references": "error",
		"require-sri": "error",
	},
};

---
docType: rules
name: Available rules
---

Rules with <span class="fa fa-check"></span> are enabled by
`html-validate:recommended`.<br>
Rules with <span class="fa fa-file-text-o"></span> are enabled by
`html-validate:document`.
